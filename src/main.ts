import fs from "fs";
import { People } from "./people";

const FILE = "./jsons/philips.json";

const main = async () => {
    const file = await fs.promises.readFile(FILE);
    const people = JSON.parse(file.toString()) as People;
    console.log(people.results.length);
    for (const person of people.results) {
        let personData = `${person.person.name[0].formattedName} - ${person.person.email[0].value}`;
        personData = `${personData} - ${person.person.photo[0].url}`;

        console.log(personData);
    }
};

main().then();
