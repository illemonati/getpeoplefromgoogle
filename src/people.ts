export interface Affinity {
    loggingId: string;
}

export interface SourceId {
    container: string;
    id: string;
    containerType: string;
}

export interface IdentityInfo {
    sourceIds: SourceId[];
}

export interface Metadata {
    ownerId: string;
    objectType: string;
    inViewerDomain: boolean;
    lastUpdateTimeMicros: string;
    affinity: Affinity[];
    identityInfo: IdentityInfo;
}

export interface Metadata2 {
    container: string;
    encodedContainerId: string;
    containerType: string;
}

export interface Name {
    metadata: Metadata2;
    displayName: string;
    formattedName: string;
    givenName: string;
    familyName: string;
    unstructuredName: string;
}

export interface Metadata3 {
    container: string;
    encodedContainerId: string;
    containerType: string;
}

export interface Photo {
    metadata: Metadata3;
    url: string;
    photoToken: string;
    isDefault?: boolean;
    isMonogram?: boolean;
    monogramBackground: string;
}

export interface Metadata4 {
    container: string;
    primary: boolean;
    verified: boolean;
    encodedContainerId: string;
    containerType: string;
}

export interface ExtendedData {
    smtpServerSupportsTls: boolean;
}

export interface Email {
    metadata: Metadata4;
    value: string;
    extendedData: ExtendedData;
}

export interface ReadOnlyProfileInfo {
    ownerId: string;
    inViewerDomain: boolean;
}

export interface Metadata5 {
    container: string;
    encodedContainerId: string;
    containerType: string;
}

export interface Organization {
    metadata: Metadata5;
    department: string;
    title: string;
    type: string;
}

export interface Person {
    personId: string;
    metadata: Metadata;
    name: Name[];
    photo: Photo[];
    email: Email[];
    readOnlyProfileInfo: ReadOnlyProfileInfo[];
    organization: Organization[];
}

export interface Result {
    suggestion: string;
    objectType: string;
    person: Person;
}

export interface Status {}

export interface People {
    results: Result[];
    nextPageToken: string;
    status: Status;
}
